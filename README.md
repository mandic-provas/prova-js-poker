# Prova de JavaScript - Mandic

Poker é um jogo de baralho de habilidade e raciocinio onde o jogador com a maior mão é o vencedor.
Uma mão é composta por 5 cartas, e existe um ranking de combinações que define quem tem a maior mão.

## Nomenclatura para o teste

Para este teste iremos utilizar o valor da carta seguido pelo naipe, que segue a seguinte nomenclatura:
C - Copas
E - Espada
O - Ouro
P - Paus

Sendo assim, uma mão pode ser representada como: 2C5OAPAC10E (2 de copas, 5 de ouro, às de paus, às de copa e 10 de espada)

## Ranking de mãos do poker (do maior para o menor)

### Royal Flush
Composta por 10, valete, Dama, Rei e As do mesmo naipe; Não há critério de desempate. 

Ex. 
10CJCQCKCAC
10EJEQEKEAE

### Straight Flush
Composta por uma sequencia do mesmo naipe. Em caso de uma mão similar, o valor da carta mais alta da sequência vence;

Ex. 
```
4E5E6E7E8E
7C8C9C10CJC
```

Em caso de empate, 7C8C9C10CJC vence 4E5E6E7E8E, pois JC é maior que 8E.

### Quadra
Composta por 4 cartas do mesmo valor. Em caso de empate, o jogador com a maior carta restante ('kicker') vence;

Ex.
```
8C7E7C7O7P
AE4E4C4O4P
```
Em caso de empate, AE4E4C4O4P vence 8C7E7C7O7P, pois AE é maior que 8C.

### Full House
Composta por três cartas do mesmo valor, e duas cartas do mesmo valor. Em caso de empate, as três cartas de mesmo valor mais altas vencem.
Ex.
```
7C7PKOKEKP
3C3PAOAEAP
```

Em caso de empate, 3E3OACAEAP vence 7C7PKOKEKP, pois ACAEAP é maior que KOKEKP.

### Flush
Cinco cartas do mesmo naipe, não em sequência. Em caso de empate, o jogador com a maior carta de maior valor vence.

Ex.
```
AC7C2C9C10C
8P7PQP3PKP
AE5E2E3EKE
```

Em caso de empate, AC7C2C9C10C vence 8P7PQP3PKP, pois AC é maior que KP.
Em caso de empate, AE5E2E3EKE vence AC7C2C9C10C, pois AC é igual AE, porém KE (segunda maior) é maior que 10C.

### Sequência
Cinco cartas de naipes diferentes em sequência. Em caso de uma mão similar, o valor da carta mais alta da sequência vence.

Ex.
```
2C3E4P5C6C
8E9E10EJEQE
```

Em caso de empate 8E9E10EJEQE vence 2C3E4P5C6C, pois QE é maior que 6C.

### Trinca
Três cartas do mesmo valor, e duas outras cartas não relacionadas. Em caso de empate, o jogador com a maior carta restante, e caso necessário a segunda mais alta carta restante ('kicker') vence.

Ex.
```
JEJOJC5E8P
9O9E9P3CKE
7E7O7PKOQP
```

Em caso de empate 9O9E9P3CKE vence JEJOJC5E8P, pois KE é maior que 8P.
Em caso de empate 7E7O7PKOQP vence 9O9E9P3CKE, pois KE é igual KO, e QP (segunda maior) é maior que 3C.

### Dois pares
Duas cartas de mesmo valor e mais duas cartas diferentes de mesmo valor, além do kicker. No caso dos jogadores terem Dois Pares idênticos, o maior kicker vence.

Ex.
```
6E6P10P10OKC
8E8O9E9P2P
```

Em caso de empate 6E6P10P10OKC vence 8E8O9E9P2P, pois KC é maior que 2P;

### Par
Duas cartas do mesmo valor, e três outras cartas não relacionadas. Em caso de empate, o jogador com a maior carta restante, e caso necessário a segunda ou terceira carta restante mais alta ('kicker') vence.

Ex.
```
ACAPKO6E8C
10P10EKEQC2C
7C7PACKP4O
7P7OKPQO3C
```

Em caso de empate 7C7PACKP4O vence 10P10EKEQC2C, pois AC é maior que KP.
Em caso de empate 10P10EKEQC2C vence ACAPKO6E8C, pois KE é igual KO, e QC (segunda maior) é maior que 8C.
Em caso de empate 7P7OKPQO10C vence 10P10EKEQC2C, pois KP é igual KE, QO (segunda maior) é igual QC, e 10C é maior que 2C.

### Carta Alta
Qualquer mão que não esteja nas categorias acima. Em caso de empate, a carta mais alta vence.

Ex.
```
KOJP10C6C3O
9P8PAO10PQE
```

9P8PAO10PQE vence KOJP10C6C3O, pois AO é maior que KO; 

## Tarefa

Criar um código que deve ser capaz de receber duas mãos de poker, e retornar qual é a mão vencedora, conforme as regras do poker citadas anteriormente.
Deve ser implementado em Javascript ou Typescript.
A entrada das mãos é uma String com as mãos separadas por um espaço em branco. As mãos estão definidas seguindo a mesma nomenclatura utilizada nos exemplos anteriores.
A saida deve ser também uma string, contendo a mão que venceu e qual o nome da mão vencedora.

Exemplos:

```
Entrada: JEJOJC5E8P KOJP10C6C3O
Saida: Mão 1 - Trinca

Entrada: 10P10EKEQC2C 7C7PACKP4O 9P8PAO10PQE
Saída: Mão 2 - Par

Entrada: 8C7E7C7O7P 8P5E5C5O5P
Saída: Empate
```

O sistema deve possuir

- API
- Frontend


### API

Deve-se criar uma API simples que permita receber as mãos, e retornar o resultado.
**O formato de entrada e saída será de livre escolha do candidato.**

O erro deve ser tratado retornando o código adequado ao erro encontrado.

### Frontend

O frontend deve receber a string com a jogada, processá-la e exibir a mão vencedora.
Deve-se também exibir as mãos de forma visual, com o valor e o naipe correspondente.

# Avaliação

Será avaliado a codificação a nivel de estrutura de dados e organização de código.

A implementação deve conter:

- README informando detalhes da implementação;
- Alta de cobertura de testes automatizados;
- Validação da entrada de dados, retornando a informação do erro encontrado, caso possua.

É livre para o usuário inserir frameworks e libs para apoio na implementação.

O código implementado deve ser entregue em alguma ferramenta de controle de versão.